/**
 * 
 */
package uk.ac.qub.cleaner;

/**
 * Paul McGlinchey - 40181899
 *
 */
public enum FuelType {
	PETROL, DIESEL, ELECTRICITY, PETROL_HYBRID, CNG, DIESEL_HYBRID, LPG
}
