/**
 * 
 */
package uk.ac.qub.cleaner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

/**
 * @author Paul McGlinchey - 40181899
 *
 */
public class CleanedDataWriter {
	
	public static void writer(Collection<Record> records) {
		
		try {
			
			File file = new File("cleaned-data-gaea.csv");
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(file, false));
			String output;
			
			int lineCount = 0;
			
			for (Record r : records) {
				if (lineCount % 2 == 0) {
					output = r.toCSV();
					bw.write(output);					
				}
				
				if (lineCount > 20000) {
					break;
				}
				lineCount++;
			}
			
			System.out.println("DONE");
			
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
