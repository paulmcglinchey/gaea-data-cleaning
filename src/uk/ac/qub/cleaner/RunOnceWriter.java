/**
 * 
 */
package uk.ac.qub.cleaner;

import java.util.ArrayList;
import java.util.Collection;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.File;

/**
 * @author Paul McGlinchey - 40181899
 *
 */
public abstract class RunOnceWriter {

	public static void outputSQL(Collection<Record> records) {
		String sql_result = "";
		
		sql_result += populateManufacturerRows(records);
		sql_result += populateVehicleFuelType(records);
		sql_result += populateVehicleTransmissionType(records);
		sql_result += populateVehicleYears(records);
		sql_result += populateVehicleTransmission(records);
		sql_result += populateVehicleDetail(records);
		sql_result += populateVehicleEngine(records);
		sql_result += populateVehicleEmissions(records);
		sql_result += populateVehicleEconomy(records);
		sql_result += populateVehicle(records);
		
		try {
			
			File file = new File("gaea.sql");
			
			BufferedWriter br = new BufferedWriter(new FileWriter(file, false));
			br.write(sql_result);
			
			br.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("DONE");
		
	}
	
	public static String replaceLastLineEnder(String line) {
		StringBuilder replacer = new StringBuilder(line);
		
		replacer.setCharAt(line.lastIndexOf(','), ';');
		
		line = String.valueOf(replacer);
		
		return line;
	}
	
	public static String populateManufacturerRows(Collection<Record> records) {
		String result = "";
		Collection<String> manufacturers = new ArrayList<>(); 
		
		result += "\nINSERT INTO `gaea_vehiclemanufacturer` (Manufacturer) VALUES";
		
		for (Record r : records) {
			String current = r.getManufacturer().toUpperCase();
			if (!manufacturers.contains(current)) {
				manufacturers.add(current);
				result += "\n('" + current + "'),";
			}
		}
		
		return replaceLastLineEnder(result);
	}
	
	public static String populateVehicleFuelType(Collection<Record> records) {
		String result = "";
		
		Collection<String> fuelTypes = new ArrayList<>();
		
		result += "\nINSERT INTO `gaea_vehiclefueltype` (FuelType) VALUES";
		
		for (Record r : records) {
			
			String fuelType = String.valueOf(r.getFuelType());
			
			if (!fuelTypes.contains(fuelType)) {
				fuelTypes.add(fuelType);
				result += "\n('" + fuelType + "'),";
			}
		}
		
		return replaceLastLineEnder(result);
	}
	
	public static String populateVehicleTransmissionType(Collection<Record> records) {
		String result = "";
		
		Collection<String> transmissionTypes = new ArrayList<>();
		
		result += "\nINSERT INTO `gaea_vehicletransmissiontype` (TransmissionType) VALUES";
		
		for (Record r : records) {
			
			String transmissionType = String.valueOf(r.getTransmissionType());
			
			if (!transmissionTypes.contains(transmissionType)) {
				transmissionTypes.add(transmissionType);
				result += "\n('" + transmissionType + "'),";
			}
		}
		
		return replaceLastLineEnder(result);
	}
	
	public static String populateVehicleYears(Collection<Record> records) {
		String result = "";
		
		Collection<Integer> years = new ArrayList<>();
		
		result += "\nINSERT INTO `gaea_vehicleyear` (Year) VALUES";
		
		for (Record r : records) {
			if (!years.contains(r.getYear())) {
				years.add(r.getYear());
				result += "\n('" + String.valueOf(r.getYear()) + "'),";
			}
		}
		
		return replaceLastLineEnder(result);
	}
	
	public static String populateVehicleTransmission(Collection<Record> records) {
		String result = "";
		
		Collection<String> transmissions = new ArrayList<>();
		
		result += "\nINSERT INTO `gaea_vehicletransmission` (TransmissionTypeID, Transmission) VALUES";
		
		for (Record r : records) {
			if (!transmissions.contains(r.getTransmission())) {
				transmissions.add(r.getTransmission());
				result += "\n((SELECT TransmissionTypeID from gaea_vehicletransmissiontype WHERE TransmissionType='" + r.getTransmissionType() + "'), '" + r.getTransmission() + "'),";
			}
		}
		
		return replaceLastLineEnder(result);
	}
	
	public static String populateVehicleDetail(Collection<Record> records) {

		String result = "";
		
		Collection<String> details = new ArrayList<>();
		
		result += "\nINSERT INTO `gaea_vehicledetail` (ManufacturerID, Model, Description) VALUES";
		
		for (Record r : records) {
			if (!details.contains(r.getModel())) {
				details.add(r.getModel());
				result += "\n((SELECT ManufacturerID from gaea_vehiclemanufacturer WHERE Manufacturer='" + r.getManufacturer() + "'), '" + r.getModel() + "', '" + r.getDescription() + "'),"; 
			}
		}
		
		return replaceLastLineEnder(result);
	}
	
	public static String populateVehicleEngine(Collection<Record> records) {
		String result = "";
		
		Collection<Integer> engines = new ArrayList<>();
		
		result += "\nINSERT INTO `gaea_vehicleengine` (FuelTypeID, EngineSize) VALUES";
		
		for (Record r : records) {
			if (!engines.contains(r.getEngineCap())) {
				engines.add(r.getEngineCap());
				result += "\n((SELECT FuelTypeID from gaea_vehiclefueltype WHERE FuelType='" + r.getFuelType() + "'), " + r.getEngineCap() + "),";
			}
		}
		
		return replaceLastLineEnder(result);
	}
	
	public static String populateVehicleEmissions(Collection<Record> records) {
		String result = "";
		
		Collection<Double[]> emissions = new ArrayList<>();
		
		result += "\nINSERT INTO `gaea_vehicleemissions` (NoiseLevel, CO2, CO, NOX) VALUES";
		
		for (Record r : records) {
			Double[] current = {r.getNoiseLevel(), r.getCO2(), r.getCO(), r.getNOX()};
			if (!emissions.contains(current)) {
				emissions.add(current);
				result += "\n('" + current[0] + "', '" + current[1] + "', '" + current[2] + "', '" + current[3] + "'),";
			}
		}
		
		return replaceLastLineEnder(result);
	}
	
	public static String populateVehicleEconomy(Collection<Record> records) {
		String result = "";
		
		Collection<Double[]> economies = new ArrayList<>();
		
		result += "\nINSERT INTO `gaea_vehicleeconomy` (UrbanMetric, ExtraUrbanMetric, CombinedMetric, UrbanImperial, ExtraUrbanImperial, CombinedImperial, FuelCost12000Miles) VALUES";
		
		for (Record r : records) {
			Double[] current = {r.getUrbanMetric(), r.getExtraUrbanMetric(), r.getCombinedMetric(), r.getUrbanImperial(), r.getExtraUrbanImperial(), r.getCombinedImperial(), r.getFuelCost12000Miles()};
			if (!economies.contains(current)) {
				economies.add(current);
				result += "\n('" + current[0] + "', '" + current[1] + "', '" + current[2] + "', '" + current[3] + "', '" + current[4] + "', '" + current[5] + "', '" + current[6] + "'),";
			}
		}
		
		return replaceLastLineEnder(result);
	}
	
	public static String populateVehicle(Collection<Record> records) {
		String result = "";
		
		result += "\nINSERT INTO `gaea_vehicle` (VehicleDetailID, YearID, TransmissionID, EngineID, TaxBand, EuroStandard, EconomyID, EmissionsID) VALUES";
		
		for (Record r : records) {
			
			Double[] economies = {r.getUrbanMetric(), r.getExtraUrbanMetric(), r.getCombinedMetric(), r.getUrbanImperial(), r.getExtraUrbanImperial(), r.getCombinedImperial()};
			Double[] emissions = {r.getNoiseLevel(), r.getCO2(), r.getCO(), r.getNOX()};
			
			result += 	"\n((SELECT VehicleDetailID from gaea_vehicledetail WHERE Model='" + r.getModel() + "'), "
						+ "(SELECT YearID from gaea_vehicleyear WHERE Year='" + r.getYear() + "'), "
						+ "(SELECT TransmissionID from gaea_vehicletransmission WHERE Transmission='" + r.getTransmission() + "'), "		
						+ "(SELECT EngineID from gaea_vehicleengine WHERE EngineSize='" + r.getEngineCap() + "'), " + r.getTaxBand() + ", " + r.getEuroStandard() 
						+ ", (SELECT EconomyID from gaea_vehicleeconomy WHERE UrbanMetric='" + economies[0] + "'), " 
						+ "(SELECT EmissionsID from gaea_vehicleemissions WHERE NoiseLevel='" + emissions[0] + "'),";
		}
		
		return replaceLastLineEnder(result);
	}
	
}
