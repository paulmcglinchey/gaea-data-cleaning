/**
 * 
 */
package uk.ac.qub.cleaner;

import java.util.Random;

/**
 * Paul McGlinchey - 40181899
 *
 */
public class TaxBandGenerator {
	
	public static char generate() {
		char[] bands = {'A','B','C','D','E','F','G','H','I','J','K','L','M'};
		char choice;
		
		Random rand = new Random();
		choice = bands[rand.nextInt(bands.length - 1)];
		
		return choice;
	}
}
