/**
 * 
 */
package uk.ac.qub.cleaner;

/**
 * Paul McGlinchey - 40181899
 *
 */
public enum TransmissionType {
	AUTOMATIC, MANUAL, NA
}
