/**
 * 
 */
package uk.ac.qub.cleaner;

/**
 * Paul McGlinchey - 40181899
 *
 */
public class Record {

	// instance vars
	private int year;
	private String manufacturer;
	private String model;
	private String description;
	private int euroStandard;
	private char taxBand;
	private String transmission;
	private TransmissionType transmissionType;
	private int engineCap;
	private FuelType fuelType;
	private double urbanMetric;
	private double extraUrbanMetric;
	private double combinedMetric;
	private double urbanImperial;
	private double extraUrbanImperial;
	private double combinedImperial;
	private double noiseLevel;
	private double CO2;
	private double CO;
	private double NOX;
	private double fuelCost12000Miles;
	
	// constructors
	
	/**
	 * Default constructor
	 * @return
	 */
	public Record() {};
	
	/**
	 * Constructor with args
	 * @return
	 */
	public Record(
			int year, 
			String manufacturer, 
			String model, 
			String description, 
			int euroStandard, 
			char taxBand, 
			String transmission,
			String transmissionType,
			int engineCap,
			String fuelType,
			double urbanMetric,
			double extraUrbanMetric,
			double combinedMetric,
			double urbanImperial,
			double extraUrbanImperial,
			double combinedImperial,
			double noiseLevel,
			double CO2,
			double CO,
			double NOX,
			double fuelCost12000Miles) {
		this.setYear(year);
		this.setModel(model);
		this.setManufacturer(manufacturer);
		this.setDescription(description);
		this.setEuroStandard(euroStandard);
		this.setTaxBand(taxBand);
		this.setTransmission(transmission);
		this.setTransmissionTypeString(transmissionType);
		this.setEngineCap(engineCap);
		this.setFuelTypeString(fuelType);
		this.setUrbanMetric(urbanMetric);
		this.setExtraUrbanMetric(extraUrbanMetric);
		this.setCombinedMetric(combinedMetric);
		this.setUrbanImperial(urbanImperial);
		this.setExtraUrbanImperial(extraUrbanImperial);
		this.setCombinedImperial(combinedImperial);
		this.setNoiseLevel(noiseLevel);
		this.setCO2(CO2);
		this.setCO(CO);
		this.setNOX(NOX);
		this.setFuelCost12000Miles(fuelCost12000Miles);
	}
	
	// methods
	public String toCSV() {
		
		String sep = ",";
		
		return (
				this.getYear() + sep +
				this.getManufacturer() + sep +
				this.getModel() + sep +
				this.getDescription() + sep +
				this.getEngineCap() + sep +
				this.getFuelType() + sep +
				this.getEuroStandard() + sep +
				this.getTaxBand() + sep +
				this.getTransmission() + sep +
				this.getTransmissionType() + sep +
				this.getUrbanMetric() + sep +
				this.getExtraUrbanMetric() + sep +
				this.getCombinedMetric() + sep +
				this.getUrbanImperial() + sep +
				this.getExtraUrbanImperial() + sep +
				this.getCombinedImperial() + sep +
				this.getFuelCost12000Miles() + sep +
				this.getNoiseLevel() + sep +
				this.getCO2() + sep +
				this.getCO() + sep +
				this.getNOX() + "\n"
		);
				
	}
	
	// getters and setters
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if (year == 0) {
			this.year = 2012;
		} else {
			this.year = year;			
		}
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getEuroStandard() {
		return euroStandard;
	}
	public void setEuroStandard(int euroStandard) {
		this.euroStandard = euroStandard;
	}
	public char getTaxBand() {
		return taxBand;
	}
	public void setTaxBand(char taxBand) {
		if (taxBand == ' ') {
			this.taxBand = TaxBandGenerator.generate();
		} else {			
			this.taxBand = taxBand;
		}
	}
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}
	public TransmissionType getTransmissionType() {
		return transmissionType;
	}
	public void setTransmissionType(TransmissionType transmissionType) {
		this.transmissionType = transmissionType;
	}
	public void setTransmissionTypeString(String transmissionType) throws IllegalArgumentException, NullPointerException {
		if (transmissionType == null) {
			throw new NullPointerException("transmissionType CANNOT BE NULL");
		}
		
		if (!transmissionType.trim().isEmpty()) {
			switch (transmissionType.trim().toUpperCase()) {
			case "MANUAL": {
				this.transmissionType = TransmissionType.MANUAL;
				break;
			}
			case "AUTOMATIC": {
				this.transmissionType = TransmissionType.AUTOMATIC;
				break;
			}
			case "N/A": {
				this.transmissionType = TransmissionType.NA;
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + transmissionType);
			}
		} else {
			this.transmissionType = TransmissionType.MANUAL;
		}
	}
	public int getEngineCap() {
		return engineCap;
	}
	public void setEngineCap(int engineCap) {
		this.engineCap = engineCap;
	}
	public FuelType getFuelType() {
		return fuelType;
	}
	public void setFuelType(FuelType fuelType) {
		this.fuelType = fuelType;
	}
	public void setFuelTypeString(String fuelType) throws IllegalArgumentException, NullPointerException {
		if (fuelType == null) {
			throw new NullPointerException("fuelType CANNOT BE NULL");
		}
		
		if (!fuelType.trim().isEmpty()) {
			switch (fuelType.trim().toUpperCase()) {
			case "PETROL": {
				this.fuelType = FuelType.PETROL;
				break;
			}
			case "DIESEL": {
				this.fuelType = FuelType.DIESEL;
				break;
			}
			case "PETROL HYBRID": {
				this.fuelType = FuelType.PETROL_HYBRID;
				break;
			}
			case "ELECTRICITY": {
				this.fuelType = FuelType.ELECTRICITY;
				break;
			}
			case "CNG": {
				this.fuelType = FuelType.CNG;
				break;
			}
			case "PETROL / E85 (FLEX FUEL)": {
				this.fuelType = FuelType.PETROL;
				break;
			}
			case "PETROL ELECTRIC": {
				this.fuelType = FuelType.PETROL_HYBRID;
				break;
			}
			case "DIESEL ELECTRIC": {
				this.fuelType = FuelType.DIESEL_HYBRID;
				break;
			}
			case "ELECTRICITY/PETROL": {
				this.fuelType = FuelType.PETROL_HYBRID;
				break;
			}
			case "ELECTRICITY/DIESEL": {
				this.fuelType = FuelType.DIESEL_HYBRID;
				break;
			}
			case "LPG": {
				this.fuelType = FuelType.LPG;
				break;
			}
			case "LPG / PETROL": {
				this.fuelType = FuelType.LPG;
				break;
			}
			case "PETROL / E85": {
				this.fuelType = FuelType.PETROL;
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + fuelType.trim());
			}
		} else {
			throw new IllegalArgumentException("fuelType CANNOT BE EMPTY");
		}
	}
	public double getUrbanMetric() {
		return urbanMetric;
	}
	public void setUrbanMetric(double urbanMetric) {
		this.urbanMetric = urbanMetric;
	}
	public double getExtraUrbanMetric() {
		return extraUrbanMetric;
	}
	public void setExtraUrbanMetric(double extraUrbanMetric) {
		this.extraUrbanMetric = extraUrbanMetric;
	}
	public double getCombinedMetric() {
		return combinedMetric;
	}
	public void setCombinedMetric(double combinedMetric) {
		this.combinedMetric = combinedMetric;
	}
	public double getUrbanImperial() {
		return urbanImperial;
	}
	public void setUrbanImperial(double urbanImperial) {
		this.urbanImperial = urbanImperial;
	}
	public double getExtraUrbanImperial() {
		return extraUrbanImperial;
	}
	public void setExtraUrbanImperial(double extraUrbanImperial) {
		this.extraUrbanImperial = extraUrbanImperial;
	}
	public double getCombinedImperial() {
		return combinedImperial;
	}
	public void setCombinedImperial(double combinedImperial) {
		this.combinedImperial = combinedImperial;
	}
	public double getNoiseLevel() {
		return noiseLevel;
	}
	public void setNoiseLevel(double noiseLevel) {
		this.noiseLevel = noiseLevel;
	}
	public double getCO2() {
		return CO2;
	}
	public void setCO2(double cO2) {
		CO2 = cO2;
	}
	public double getCO() {
		return CO;
	}
	public void setCO(double cO) {
		CO = cO;
	}
	public double getNOX() {
		return NOX;
	}
	public void setNOX(double nOX) {
		NOX = nOX;
	}
	public double getFuelCost12000Miles() {
		return fuelCost12000Miles;
	}
	public void setFuelCost12000Miles(double fuelCost12000Miles) {
		this.fuelCost12000Miles = fuelCost12000Miles;
	}
	
}
