/**
 * 
 */
package uk.ac.qub.cleaner;

/**
 * Paul McGlinchey - 40181899
 *
 */
public class DescriptionOutput {
	// instance vars
	private String desc;
	private int colsToSkip;
	
	// constructor
	public DescriptionOutput() {};
	
	public DescriptionOutput(String desc, int colsToSkip) {
		this.setDesc(desc);
		this.setColsToSkip(colsToSkip);
	}
	
	// getters and setters
	public String getDesc() {
		return this.desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public int getColsToSkip() {
		return this.colsToSkip;
	}
	public void setColsToSkip(int colsToSkip) {
		this.colsToSkip = colsToSkip;
	}
}
