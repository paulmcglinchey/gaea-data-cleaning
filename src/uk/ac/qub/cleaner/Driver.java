/**
 * 
 */
package uk.ac.qub.cleaner;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

/**
 * Paul McGlinchey - 40181899
 *
 */
public class Driver {
	
	public static int colsToSkip = 0;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<Record> records = (ArrayList<Record>) readAndGetRecords(new File("data.csv"), true);
		System.out.println(records.size());
		
		CleanedDataWriter.writer(records);
	}
	
	/**
	 * Takes the file to be parsed a returns an ArrayList of records for each entry
	 * @param file
	 * @param hasHeader
	 * @return
	 */
	public static Collection<Record> readAndGetRecords(File file, boolean hasHeader) {
		Collection<Record> records = new ArrayList<Record>();
		
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			String line = br.readLine();
			String[] splitLine;
	
			while (line != null) {
					if (hasHeader) {
						hasHeader = false;
					} else {
						System.out.println(line);
						splitLine = line.split(",");
						
						if (splitLine.length >= 25) {
						
						records.add(new Record(
								Integer.parseInt(splitLine[1 + colsToSkip]),
								splitLine[2 + colsToSkip],
								trimModelEntries(splitLine),
								trimDescEntries(splitLine).getDesc(),
								Integer.parseInt(splitLine[5 + colsToSkip]),
								!splitLine[6 + colsToSkip].isBlank() ? splitLine[6 + colsToSkip].charAt(0) : ' ',
								splitLine[7 + colsToSkip],
								splitLine[8 + colsToSkip],
								!splitLine[9 + colsToSkip].isBlank() ? Integer.parseInt(splitLine[9 + colsToSkip]) : 0,
								splitLine[10 + colsToSkip],
								!splitLine[11 + colsToSkip].isBlank() ? Double.parseDouble(splitLine[11 + colsToSkip]) : 7.84,
								!splitLine[12 + colsToSkip].isBlank() ? Double.parseDouble(splitLine[12 + colsToSkip]) : 4.5,
								!splitLine[13 + colsToSkip].isBlank() ? Double.parseDouble(splitLine[13 + colsToSkip]) : 5.88,
								!splitLine[14 + colsToSkip].isBlank() ? Double.parseDouble(splitLine[14 + colsToSkip]) : 30,
								!splitLine[15 + colsToSkip].isBlank() ? Double.parseDouble(splitLine[15 + colsToSkip]) : 50,
								!splitLine[16 + colsToSkip].isBlank() ? Double.parseDouble(splitLine[16 + colsToSkip]) : 40,
								!splitLine[17 + colsToSkip].isBlank() ? Double.parseDouble(splitLine[17 + colsToSkip]) : 0,
								!splitLine[18 + colsToSkip].isBlank() ? Double.parseDouble(splitLine[18 + colsToSkip]) : 0,
								!splitLine[20 + colsToSkip].isBlank() ? Double.parseDouble(splitLine[20 + colsToSkip]) : 0,
								!splitLine[21 + colsToSkip].isBlank() && splitLine[21 + colsToSkip] != null ? Double.parseDouble(splitLine[21 + colsToSkip]) : 0,
								mapFuelCost(splitLine)
								));
						
						}
						
						colsToSkip = 0;
						
					}
				
				line = br.readLine();
			}
			
			
			br.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return records;
	}
	
	public static String trimModelEntries(String[] splitLine) {
		String model = "";
		int index = 3 + colsToSkip;
		
		if (splitLine[index].charAt(0) == '"') {
			
			model += splitLine[index].replaceAll("\"", "");
			
			do {
				index++;
				if (splitLine[index].charAt(0) == ' ' || splitLine[index].charAt(splitLine[index].length() - 1) == '"') {
					model += splitLine[index].replaceAll("\"", "");
					colsToSkip++;
					System.out.println(model + " " + colsToSkip);
				} else {
					System.out.println(splitLine[index].charAt(splitLine[index].length() - 1));
					break;
				}
			} while (true);
			
		} else {
			model += splitLine[index];
		}
		
		model = model.replaceAll("'", "");
		
		return model;
	}
	
	public static DescriptionOutput trimDescEntries(String[] splitLine) {
		String desc = "";
		int index = 4 + colsToSkip;
		
		if (splitLine[index].charAt(0) == '"') {
			
			desc += splitLine[index].replaceAll("\"", "");
			
			do {
				index++;
				if (splitLine[index].charAt(0) == ' ') {
					desc += splitLine[index].replaceAll("\"", "");
					colsToSkip++;
					System.out.println(desc + " " + colsToSkip);
				} else if (splitLine[index].charAt(splitLine[index].length() - 1) == '"') {
					desc += splitLine[index].replaceAll("\"", "");
					colsToSkip++;
					System.out.println(desc + " " + colsToSkip);
					break;
				} else {
					System.out.println(splitLine[index].charAt(splitLine[index].length() - 1));
					break;
				}
			} while (true);
			
		} else {
			desc += splitLine[index];
		}
		
		desc = desc.replaceAll("'", "");
		
		return new DescriptionOutput(desc, colsToSkip);
	}
	
	public static double mapFuelCost(String[] splitLine) {
		int index = 24 + colsToSkip;
		double fuelCost12000 = 0;
		Random rand = new Random();
		
		try {
			if (!splitLine[index].isEmpty()) {
				fuelCost12000 = Double.parseDouble(splitLine[index]);
			} else {
				fuelCost12000 = rand.nextInt(2000) + 1000;
			}			
		} catch (Exception e) {
			fuelCost12000 = rand.nextInt(2000) + 1000;
		}
		
		return fuelCost12000;
	}
}
