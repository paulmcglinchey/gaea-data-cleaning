-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2021 at 03:17 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehicle`
--

CREATE TABLE `gaea_vehicle` (
  `VehicleID` int(11) NOT NULL,
  `VehicleDetailID` int(11) NOT NULL,
  `YearID` int(11) NOT NULL,
  `TransmissionID` int(11) NOT NULL,
  `EngineID` int(11) NOT NULL,
  `TaxBand` varchar(1) NOT NULL,
  `EuroStandard` int(11) NOT NULL,
  `EconomyID` int(11) NOT NULL,
  `EmissionsID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehicledetail`
--

CREATE TABLE `gaea_vehicledetail` (
  `VehicleDetailID` int(11) NOT NULL,
  `ManufacturerID` int(11) NOT NULL,
  `Model` varchar(255) NOT NULL,
  `Description` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehicleeconomy`
--

CREATE TABLE `gaea_vehicleeconomy` (
  `EconomyID` int(11) NOT NULL,
  `UrbanMetric` decimal(11,2) NOT NULL,
  `ExtraUrbanMetric` decimal(11,2) NOT NULL,
  `CombinedMetric` decimal(11,2) NOT NULL,
  `UrbanImperial` decimal(11,2) NOT NULL,
  `ExtraUrbanImperial` decimal(11,2) NOT NULL,
  `CombinedImperial` decimal(11,2) NOT NULL,
  `FuelCost12000Miles` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehicleemissions`
--

CREATE TABLE `gaea_vehicleemissions` (
  `EmissionsID` int(11) NOT NULL,
  `NoiseLevel` decimal(11,2) NOT NULL,
  `CO2` decimal(11,2) NOT NULL,
  `CO` decimal(11,2) NOT NULL,
  `NOX` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehicleengine`
--

CREATE TABLE `gaea_vehicleengine` (
  `EngineID` int(11) NOT NULL,
  `FuelTypeID` int(11) NOT NULL,
  `EngineSize` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehiclefueltype`
--

CREATE TABLE `gaea_vehiclefueltype` (
  `FuelTypeID` int(11) NOT NULL,
  `FuelType` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehiclemanufacturer`
--

CREATE TABLE `gaea_vehiclemanufacturer` (
  `ManufacturerID` int(11) NOT NULL,
  `Manufacturer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehicletransmission`
--

CREATE TABLE `gaea_vehicletransmission` (
  `TransmissionID` int(11) NOT NULL,
  `TransmissionTypeID` int(11) NOT NULL,
  `Transmission` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehicletransmissiontype`
--

CREATE TABLE `gaea_vehicletransmissiontype` (
  `TransmissionTypeID` int(11) NOT NULL,
  `TransmissionType` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gaea_vehicleyear`
--

CREATE TABLE `gaea_vehicleyear` (
  `YearID` int(11) NOT NULL,
  `Year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gaea_vehicle`
--
ALTER TABLE `gaea_vehicle`
  ADD PRIMARY KEY (`VehicleID`),
  ADD KEY `FK_VehicleDetailVehicle` (`VehicleDetailID`),
  ADD KEY `FK_YearVehicle` (`YearID`),
  ADD KEY `FK_TransmissionVehicle` (`TransmissionID`),
  ADD KEY `FK_EngineVehicle` (`EngineID`),
  ADD KEY `FK_EconomyVehicle` (`EconomyID`),
  ADD KEY `FK_EmissionsVehicle` (`EmissionsID`);

--
-- Indexes for table `gaea_vehicledetail`
--
ALTER TABLE `gaea_vehicledetail`
  ADD PRIMARY KEY (`VehicleDetailID`),
  ADD KEY `FK_ManufacturerVehicleDetail` (`ManufacturerID`);

--
-- Indexes for table `gaea_vehicleeconomy`
--
ALTER TABLE `gaea_vehicleeconomy`
  ADD PRIMARY KEY (`EconomyID`);

--
-- Indexes for table `gaea_vehicleemissions`
--
ALTER TABLE `gaea_vehicleemissions`
  ADD PRIMARY KEY (`EmissionsID`);

--
-- Indexes for table `gaea_vehicleengine`
--
ALTER TABLE `gaea_vehicleengine`
  ADD PRIMARY KEY (`EngineID`),
  ADD KEY `FK_FuelTypeEngine` (`FuelTypeID`);

--
-- Indexes for table `gaea_vehiclefueltype`
--
ALTER TABLE `gaea_vehiclefueltype`
  ADD PRIMARY KEY (`FuelTypeID`);

--
-- Indexes for table `gaea_vehiclemanufacturer`
--
ALTER TABLE `gaea_vehiclemanufacturer`
  ADD PRIMARY KEY (`ManufacturerID`);

--
-- Indexes for table `gaea_vehicletransmission`
--
ALTER TABLE `gaea_vehicletransmission`
  ADD PRIMARY KEY (`TransmissionID`),
  ADD KEY `FK_TransmissionTypeTransmission` (`TransmissionTypeID`);

--
-- Indexes for table `gaea_vehicletransmissiontype`
--
ALTER TABLE `gaea_vehicletransmissiontype`
  ADD PRIMARY KEY (`TransmissionTypeID`);

--
-- Indexes for table `gaea_vehicleyear`
--
ALTER TABLE `gaea_vehicleyear`
  ADD PRIMARY KEY (`YearID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gaea_vehicle`
--
ALTER TABLE `gaea_vehicle`
  MODIFY `VehicleID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaea_vehicledetail`
--
ALTER TABLE `gaea_vehicledetail`
  MODIFY `VehicleDetailID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaea_vehicleeconomy`
--
ALTER TABLE `gaea_vehicleeconomy`
  MODIFY `EconomyID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaea_vehicleemissions`
--
ALTER TABLE `gaea_vehicleemissions`
  MODIFY `EmissionsID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaea_vehicleengine`
--
ALTER TABLE `gaea_vehicleengine`
  MODIFY `EngineID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaea_vehiclefueltype`
--
ALTER TABLE `gaea_vehiclefueltype`
  MODIFY `FuelTypeID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaea_vehiclemanufacturer`
--
ALTER TABLE `gaea_vehiclemanufacturer`
  MODIFY `ManufacturerID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaea_vehicletransmission`
--
ALTER TABLE `gaea_vehicletransmission`
  MODIFY `TransmissionID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaea_vehicletransmissiontype`
--
ALTER TABLE `gaea_vehicletransmissiontype`
  MODIFY `TransmissionTypeID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaea_vehicleyear`
--
ALTER TABLE `gaea_vehicleyear`
  MODIFY `YearID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gaea_vehicle`
--
ALTER TABLE `gaea_vehicle`
  ADD CONSTRAINT `FK_EconomyVehicle` FOREIGN KEY (`EconomyID`) REFERENCES `gaea_vehicleeconomy` (`EconomyID`),
  ADD CONSTRAINT `FK_EmissionsVehicle` FOREIGN KEY (`EmissionsID`) REFERENCES `gaea_vehicleemissions` (`EmissionsID`),
  ADD CONSTRAINT `FK_EngineVehicle` FOREIGN KEY (`EngineID`) REFERENCES `gaea_vehicleengine` (`EngineID`),
  ADD CONSTRAINT `FK_TransmissionVehicle` FOREIGN KEY (`TransmissionID`) REFERENCES `gaea_vehicletransmission` (`TransmissionID`),
  ADD CONSTRAINT `FK_VehicleDetailVehicle` FOREIGN KEY (`VehicleDetailID`) REFERENCES `gaea_vehicledetail` (`VehicleDetailID`),
  ADD CONSTRAINT `FK_YearVehicle` FOREIGN KEY (`YearID`) REFERENCES `gaea_vehicleyear` (`YearID`);

--
-- Constraints for table `gaea_vehicledetail`
--
ALTER TABLE `gaea_vehicledetail`
  ADD CONSTRAINT `FK_ManufacturerVehicleDetail` FOREIGN KEY (`ManufacturerID`) REFERENCES `gaea_vehiclemanufacturer` (`ManufacturerID`);

--
-- Constraints for table `gaea_vehicleengine`
--
ALTER TABLE `gaea_vehicleengine`
  ADD CONSTRAINT `FK_FuelTypeEngine` FOREIGN KEY (`FuelTypeID`) REFERENCES `gaea_vehiclefueltype` (`FuelTypeID`);

--
-- Constraints for table `gaea_vehicletransmission`
--
ALTER TABLE `gaea_vehicletransmission`
  ADD CONSTRAINT `FK_TransmissionTypeTransmission` FOREIGN KEY (`TransmissionTypeID`) REFERENCES `gaea_vehicletransmissiontype` (`TransmissionTypeID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
